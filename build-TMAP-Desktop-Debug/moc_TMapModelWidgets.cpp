/****************************************************************************
** Meta object code from reading C++ file 'TMapModelWidgets.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.8)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../TMAP/TMapModelWidgets.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TMapModelWidgets.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.8. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TMapModelWidgets_t {
    QByteArrayData data[19];
    char stringdata0[244];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TMapModelWidgets_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TMapModelWidgets_t qt_meta_stringdata_TMapModelWidgets = {
    {
QT_MOC_LITERAL(0, 0, 16), // "TMapModelWidgets"
QT_MOC_LITERAL(1, 17, 13), // "sliderChanged"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 5), // "value"
QT_MOC_LITERAL(4, 38, 13), // "bundlesActive"
QT_MOC_LITERAL(5, 52, 2), // "on"
QT_MOC_LITERAL(6, 55, 9), // "sendInTri"
QT_MOC_LITERAL(7, 65, 35), // "std::vector<std::vector<uint3..."
QT_MOC_LITERAL(8, 101, 5), // "InTri"
QT_MOC_LITERAL(9, 107, 9), // "sendFnTri"
QT_MOC_LITERAL(10, 117, 5), // "FnTri"
QT_MOC_LITERAL(11, 123, 16), // "makeIntersection"
QT_MOC_LITERAL(12, 140, 15), // "generateRegions"
QT_MOC_LITERAL(13, 156, 14), // "setMeshOpacity"
QT_MOC_LITERAL(14, 171, 16), // "setBundlesActive"
QT_MOC_LITERAL(15, 188, 19), // "setProjectionPoints"
QT_MOC_LITERAL(16, 208, 12), // "setThreshold"
QT_MOC_LITERAL(17, 221, 11), // "setDilation"
QT_MOC_LITERAL(18, 233, 10) // "setErosion"

    },
    "TMapModelWidgets\0sliderChanged\0\0value\0"
    "bundlesActive\0on\0sendInTri\0"
    "std::vector<std::vector<uint32_t> >\0"
    "InTri\0sendFnTri\0FnTri\0makeIntersection\0"
    "generateRegions\0setMeshOpacity\0"
    "setBundlesActive\0setProjectionPoints\0"
    "setThreshold\0setDilation\0setErosion"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TMapModelWidgets[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x06 /* Public */,
       4,    1,   77,    2, 0x06 /* Public */,
       6,    1,   80,    2, 0x06 /* Public */,
       9,    1,   83,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    0,   86,    2, 0x0a /* Public */,
      12,    0,   87,    2, 0x0a /* Public */,
      13,    1,   88,    2, 0x0a /* Public */,
      14,    1,   91,    2, 0x0a /* Public */,
      15,    1,   94,    2, 0x0a /* Public */,
      16,    1,   97,    2, 0x0a /* Public */,
      17,    1,  100,    2, 0x0a /* Public */,
      18,    1,  103,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, 0x80000000 | 7,   10,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,

       0        // eod
};

void TMapModelWidgets::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TMapModelWidgets *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sliderChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->bundlesActive((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->sendInTri((*reinterpret_cast< std::vector<std::vector<uint32_t> >(*)>(_a[1]))); break;
        case 3: _t->sendFnTri((*reinterpret_cast< std::vector<std::vector<uint32_t> >(*)>(_a[1]))); break;
        case 4: _t->makeIntersection(); break;
        case 5: _t->generateRegions(); break;
        case 6: _t->setMeshOpacity((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->setBundlesActive((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->setProjectionPoints((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->setThreshold((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->setDilation((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->setErosion((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TMapModelWidgets::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TMapModelWidgets::sliderChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (TMapModelWidgets::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TMapModelWidgets::bundlesActive)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (TMapModelWidgets::*)(std::vector<std::vector<uint32_t>> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TMapModelWidgets::sendInTri)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (TMapModelWidgets::*)(std::vector<std::vector<uint32_t>> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TMapModelWidgets::sendFnTri)) {
                *result = 3;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TMapModelWidgets::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_TMapModelWidgets.data,
    qt_meta_data_TMapModelWidgets,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TMapModelWidgets::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TMapModelWidgets::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TMapModelWidgets.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int TMapModelWidgets::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void TMapModelWidgets::sliderChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TMapModelWidgets::bundlesActive(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TMapModelWidgets::sendInTri(std::vector<std::vector<uint32_t>> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void TMapModelWidgets::sendFnTri(std::vector<std::vector<uint32_t>> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
