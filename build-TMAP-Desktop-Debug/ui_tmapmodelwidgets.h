/********************************************************************************
** Form generated from reading UI file 'tmapmodelwidgets.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TMAPMODELWIDGETS_H
#define UI_TMAPMODELWIDGETS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TMapModelWidgets
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QTableView *tableView;
    QVBoxLayout *verticalLayout_3;
    QSpacerItem *verticalSpacer;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_2;
    QLabel *label_4;
    QSpinBox *projectionSpinBox;
    QLabel *label_6;
    QLabel *label;
    QLabel *label_23;
    QLabel *label_5;
    QLabel *label_3;
    QLabel *label_2;
    QPushButton *m_intersectionPushButton;
    QSlider *m_opacityHorizontalSlider;
    QCheckBox *showBundlesCheckBox;
    QWidget *tab_2;
    QGridLayout *gridLayout_3;
    QPushButton *m_regionGenerationPushButton;
    QSpinBox *m_thresholdSpinBox;
    QSpinBox *m_erosionSpinBox;
    QSpinBox *m_dilationSpinBox;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_4;
    QLabel *label_13;
    QLabel *label_12;
    QLabel *label_14;
    QLabel *label_15;
    QWidget *tab_3;
    QWidget *tab_4;
    QGridLayout *gridLayout_5;
    QLabel *label_20;
    QLabel *label_18;
    QLabel *label_16;
    QLabel *label_19;
    QCheckBox *checkBox_2;
    QLabel *label_17;
    QLabel *label_21;
    QLabel *label_22;

    void setupUi(QWidget *TMapModelWidgets)
    {
        if (TMapModelWidgets->objectName().isEmpty())
            TMapModelWidgets->setObjectName(QStringLiteral("TMapModelWidgets"));
        TMapModelWidgets->resize(597, 511);
        gridLayout = new QGridLayout(TMapModelWidgets);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tableView = new QTableView(TMapModelWidgets);
        tableView->setObjectName(QStringLiteral("tableView"));

        verticalLayout->addWidget(tableView);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Maximum);

        verticalLayout_3->addItem(verticalSpacer);

        tabWidget = new QTabWidget(TMapModelWidgets);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        gridLayout_2 = new QGridLayout(tab);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label_4 = new QLabel(tab);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_2->addWidget(label_4, 3, 0, 1, 1);

        projectionSpinBox = new QSpinBox(tab);
        projectionSpinBox->setObjectName(QStringLiteral("projectionSpinBox"));

        gridLayout_2->addWidget(projectionSpinBox, 0, 1, 1, 1);

        label_6 = new QLabel(tab);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_2->addWidget(label_6, 4, 0, 1, 1);

        label = new QLabel(tab);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        label_23 = new QLabel(tab);
        label_23->setObjectName(QStringLiteral("label_23"));

        gridLayout_2->addWidget(label_23, 2, 0, 1, 1);

        label_5 = new QLabel(tab);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_2->addWidget(label_5, 3, 1, 1, 1);

        label_3 = new QLabel(tab);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 1, 1, 1, 1);

        label_2 = new QLabel(tab);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        m_intersectionPushButton = new QPushButton(tab);
        m_intersectionPushButton->setObjectName(QStringLiteral("m_intersectionPushButton"));

        gridLayout_2->addWidget(m_intersectionPushButton, 5, 0, 1, 2);

        m_opacityHorizontalSlider = new QSlider(tab);
        m_opacityHorizontalSlider->setObjectName(QStringLiteral("m_opacityHorizontalSlider"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_opacityHorizontalSlider->sizePolicy().hasHeightForWidth());
        m_opacityHorizontalSlider->setSizePolicy(sizePolicy);
        m_opacityHorizontalSlider->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(m_opacityHorizontalSlider, 2, 1, 1, 1);

        showBundlesCheckBox = new QCheckBox(tab);
        showBundlesCheckBox->setObjectName(QStringLiteral("showBundlesCheckBox"));

        gridLayout_2->addWidget(showBundlesCheckBox, 4, 1, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        gridLayout_3 = new QGridLayout(tab_2);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        m_regionGenerationPushButton = new QPushButton(tab_2);
        m_regionGenerationPushButton->setObjectName(QStringLiteral("m_regionGenerationPushButton"));

        gridLayout_3->addWidget(m_regionGenerationPushButton, 5, 0, 1, 2);

        m_thresholdSpinBox = new QSpinBox(tab_2);
        m_thresholdSpinBox->setObjectName(QStringLiteral("m_thresholdSpinBox"));

        gridLayout_3->addWidget(m_thresholdSpinBox, 1, 1, 1, 1);

        m_erosionSpinBox = new QSpinBox(tab_2);
        m_erosionSpinBox->setObjectName(QStringLiteral("m_erosionSpinBox"));

        gridLayout_3->addWidget(m_erosionSpinBox, 3, 1, 1, 1);

        m_dilationSpinBox = new QSpinBox(tab_2);
        m_dilationSpinBox->setObjectName(QStringLiteral("m_dilationSpinBox"));

        gridLayout_3->addWidget(m_dilationSpinBox, 2, 1, 1, 1);

        label_8 = new QLabel(tab_2);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_3->addWidget(label_8, 1, 0, 1, 1);

        label_9 = new QLabel(tab_2);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_3->addWidget(label_9, 2, 0, 1, 1);

        label_10 = new QLabel(tab_2);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_3->addWidget(label_10, 3, 0, 1, 1);

        groupBox = new QGroupBox(tab_2);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout_4 = new QGridLayout(groupBox);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_4->addWidget(label_13, 1, 0, 1, 1);

        label_12 = new QLabel(groupBox);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout_4->addWidget(label_12, 0, 0, 1, 1);

        label_14 = new QLabel(groupBox);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout_4->addWidget(label_14, 0, 1, 1, 1);

        label_15 = new QLabel(groupBox);
        label_15->setObjectName(QStringLiteral("label_15"));

        gridLayout_4->addWidget(label_15, 1, 1, 1, 1);


        gridLayout_3->addWidget(groupBox, 0, 0, 1, 2);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        gridLayout_5 = new QGridLayout(tab_4);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        label_20 = new QLabel(tab_4);
        label_20->setObjectName(QStringLiteral("label_20"));

        gridLayout_5->addWidget(label_20, 2, 1, 1, 1);

        label_18 = new QLabel(tab_4);
        label_18->setObjectName(QStringLiteral("label_18"));

        gridLayout_5->addWidget(label_18, 0, 1, 1, 1);

        label_16 = new QLabel(tab_4);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout_5->addWidget(label_16, 0, 0, 1, 1);

        label_19 = new QLabel(tab_4);
        label_19->setObjectName(QStringLiteral("label_19"));

        gridLayout_5->addWidget(label_19, 2, 0, 1, 1);

        checkBox_2 = new QCheckBox(tab_4);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));

        gridLayout_5->addWidget(checkBox_2, 1, 1, 1, 1);

        label_17 = new QLabel(tab_4);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_5->addWidget(label_17, 1, 0, 1, 1);

        label_21 = new QLabel(tab_4);
        label_21->setObjectName(QStringLiteral("label_21"));

        gridLayout_5->addWidget(label_21, 3, 0, 1, 1);

        label_22 = new QLabel(tab_4);
        label_22->setObjectName(QStringLiteral("label_22"));

        gridLayout_5->addWidget(label_22, 3, 1, 1, 1);

        tabWidget->addTab(tab_4, QString());

        verticalLayout_3->addWidget(tabWidget);


        verticalLayout->addLayout(verticalLayout_3);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        retranslateUi(TMapModelWidgets);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(TMapModelWidgets);
    } // setupUi

    void retranslateUi(QWidget *TMapModelWidgets)
    {
        TMapModelWidgets->setWindowTitle(QApplication::translate("TMapModelWidgets", "Form", Q_NULLPTR));
        label_4->setText(QApplication::translate("TMapModelWidgets", "Number of triangles:", Q_NULLPTR));
        label_6->setText(QApplication::translate("TMapModelWidgets", "Show bundles", Q_NULLPTR));
        label->setText(QApplication::translate("TMapModelWidgets", "Projection points:", Q_NULLPTR));
        label_23->setText(QApplication::translate("TMapModelWidgets", "Mesh opacity:", Q_NULLPTR));
        label_5->setText(QApplication::translate("TMapModelWidgets", "0", Q_NULLPTR));
        label_3->setText(QApplication::translate("TMapModelWidgets", "0", Q_NULLPTR));
        label_2->setText(QApplication::translate("TMapModelWidgets", "Step:", Q_NULLPTR));
        m_intersectionPushButton->setText(QApplication::translate("TMapModelWidgets", "Intersection", Q_NULLPTR));
        showBundlesCheckBox->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("TMapModelWidgets", "Main", Q_NULLPTR));
        m_regionGenerationPushButton->setText(QApplication::translate("TMapModelWidgets", "Generate Regions", Q_NULLPTR));
        label_8->setText(QApplication::translate("TMapModelWidgets", "Threshold:", Q_NULLPTR));
        label_9->setText(QApplication::translate("TMapModelWidgets", "Dilation", Q_NULLPTR));
        label_10->setText(QApplication::translate("TMapModelWidgets", "Erosion", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("TMapModelWidgets", "Details", Q_NULLPTR));
        label_13->setText(QApplication::translate("TMapModelWidgets", "Number of final points:", Q_NULLPTR));
        label_12->setText(QApplication::translate("TMapModelWidgets", "Number of initial points:", Q_NULLPTR));
        label_14->setText(QApplication::translate("TMapModelWidgets", "583", Q_NULLPTR));
        label_15->setText(QApplication::translate("TMapModelWidgets", "891", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("TMapModelWidgets", "Intersection", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("TMapModelWidgets", "Regions", Q_NULLPTR));
        label_20->setText(QApplication::translate("TMapModelWidgets", "PrCu", Q_NULLPTR));
        label_18->setText(QApplication::translate("TMapModelWidgets", "34", Q_NULLPTR));
        label_16->setText(QApplication::translate("TMapModelWidgets", "Number of parcels:", Q_NULLPTR));
        label_19->setText(QApplication::translate("TMapModelWidgets", "Parcel selected:", Q_NULLPTR));
        checkBox_2->setText(QString());
        label_17->setText(QApplication::translate("TMapModelWidgets", "Graph:", Q_NULLPTR));
        label_21->setText(QApplication::translate("TMapModelWidgets", "Number of connections:", Q_NULLPTR));
        label_22->setText(QApplication::translate("TMapModelWidgets", "11", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("TMapModelWidgets", "Connections", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class TMapModelWidgets: public Ui_TMapModelWidgets {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TMAPMODELWIDGETS_H
