#include "BundleSettings.h"

// C++
#include <fstream>

BundleSettings::BundleSettings(const QString& path)
{
    std::ifstream file(path.toStdString(), std::ios::in | std::ios::binary);

    if (!file.is_open()) {
        return;
    }

    std::streampos fsize = 0;
    file.seekg(0, std::ios::end);
    uint32_t num = uint32_t((file.tellg() - fsize) / 4);
    file.seekg(0);

    uint32_t num_count = 0;

    while (num_count < num) {

        uint32_t total_points; // número de puntos de cada fibra
        file.read((char*)&total_points, sizeof(uint32_t));

        //        float** Fiber = new float*[total_points]; // fibra
        std::vector<std::vector<float>> Fiber(total_points, std::vector<float>(3, 0));

        for (uint32_t k = 0; k < total_points; k++) {
            //            Fiber[k] = new float[3];

            for (uint8_t m = 0; m < 3; m++)
                file.read((char*)&Fiber[k][m], sizeof(float));
        }

        num_count = num_count + 1 + (total_points * 3);
        m_nPointsBundle.emplace_back(total_points);
        m_bundle.emplace_back(Fiber);
    }

    file.close();
}

BundleSettings::BundleSettings()
{
}

std::vector<std::vector<std::vector<float>>> BundleSettings::getBundle() const
{
    return m_bundle;
}
std::vector<uint32_t> BundleSettings::getnPointsBundle() const
{
    return m_nPointsBundle;
}

void BundleSettings::setBundle(std::vector<std::vector<std::vector<float>>> bundle)
{
    m_bundle = bundle;
}

void BundleSettings::setNPointsBundle(std::vector<uint32_t> nPoints)
{
    m_nPointsBundle = nPoints;
}
