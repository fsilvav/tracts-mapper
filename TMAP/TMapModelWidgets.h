#ifndef TMAP_MODEL_WIDGETS_H
#define TMAP_MODEL_WIDGETS_H

// Qt
#include <QWidget>

#include "MainWindow.h"

namespace Ui {
class TMapModelWidgets;
}

class TMapModelWidgets : public QWidget {
    Q_OBJECT

public:
    explicit TMapModelWidgets(QWidget* parent = nullptr,
        QList<MeshSettings*> meshes = {}, QList<BundleSettings*> bundles = {});

    ~TMapModelWidgets();

    void meshAndBundlesIntersection(float**& vertex, const uint32_t& n_vertex, uint32_t**& polygons, const uint32_t& n_polygons,
        const uint16_t& nBundles, uint32_t*& nFibers, uint16_t**& nPoints, float****& Points, const uint8_t& nPtsLine,
        std::vector<std::vector<uint32_t>>& InTri, std::vector<std::vector<uint32_t>>& FnTri, std::vector<std::vector<std::vector<float>>>& InPoints,
        std::vector<std::vector<std::vector<float>>>& FnPoints, std::vector<std::vector<uint32_t>>& fib_index);

    float** triangle_interpolation(float***& triangles, const uint8_t& N);
    float*** multiple_triangles(float***& triangles, uint16_t& len, const uint8_t polys[][3]);
    float** multiple_vertices(float**& triangle);

    bool getMeshAndFiberIntersection(float**& fiber, const uint16_t& nPoints, const uint8_t& nPtsLine, const uint8_t& N, const uint8_t& npbp, float**& index,
        const float& step, bool***& cubeNotEmpty, const std::vector<std::vector<std::vector<std::vector<uint32_t>>>>& centroidIndex,
        const std::vector<std::vector<std::vector<std::vector<std::vector<float>>>>>& almacen, float**& vertex, uint32_t**& polygons,
        uint32_t& InInd, uint32_t& FnInd, float*& InPtInt, float*& FnPtInt);

    bool getMeshAndFiberEndIntersection(float*& fiberP0, float*& fiberP1, const uint16_t& nPoints, const uint8_t& nPtsLine, const uint8_t& N, const uint8_t& npbp,
        float**& index, const float& step, bool***& cubeNotEmpty, const std::vector<std::vector<std::vector<std::vector<uint32_t>>>>& centroidIndex,
        const std::vector<std::vector<std::vector<std::vector<std::vector<float>>>>>& almacen, float**& vertex, uint32_t**& polygons, uint32_t& Ind, float*& ptInt);

    bool ray_triangle_intersection(const float ray_near[], const float ray_dir[], const float Points[][3], float& t);
    float dotProduct(const float a[], float*& b);
    float* crossProduct(const float a[], const float b[]);

    void Thresholding(const uint8_t& th, std::vector<std::vector<uint32_t>>& Tri);
    void Dilation(const uint8_t& dil, std::vector<std::vector<uint32_t>>& Tri, uint32_t**& polygons, uint32_t& len_polygons);
    void Erosion(const uint8_t& ero, std::vector<std::vector<uint32_t>>& Tri, uint32_t**& polygons);
    std::vector<uint32_t> get_edges(const std::vector<uint32_t>& Tri, uint32_t**& polygons);

signals:
    void sliderChanged(int value);
    void bundlesActive(bool on);

    void sendInTri(std::vector<std::vector<uint32_t>> InTri);
    void sendFnTri(std::vector<std::vector<uint32_t>> FnTri);

public slots:
    void makeIntersection();
    void generateRegions();
    void setMeshOpacity(int value);
    void setBundlesActive(bool on);
    void setProjectionPoints(int value);
    void setThreshold(int value);
    void setDilation(int value);
    void setErosion(int value);

private:
    void setupMeshes();
    void setupBundles();

    Ui::TMapModelWidgets* ui;
    QList<MeshSettings*> m_meshes;
    QList<BundleSettings*> m_bundles;

    float** Lvertex;
    uint32_t** Lpolygons;
    uint32_t n_Lvertex = 0, n_Lpolygons = 0;

    uint16_t m_nBundles;
    uint32_t* m_nFibers;
    uint16_t** m_nPoints;
    float**** m_Points;

    uint8_t m_nPtsLine = 2;
    uint8_t m_threshold = 1;
    uint8_t m_dilation = 6;
    uint8_t m_erosion = 3;
    float m_step = 0;

    std::vector<std::vector<uint32_t>> fib_index;
    std::vector<std::vector<uint32_t>> InTri, FnTri;
    std::vector<std::vector<std::vector<float>>> InPoints, FnPoints;
};

#endif // TMAP_MODEL_WIDGETS_H
