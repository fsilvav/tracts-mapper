#ifndef BUNDLE_SETTINGS_H
#define BUNDLE_SETTINGS_H

// QT
#include <QString>
#include <QVector>

class BundleSettings {

public:
    BundleSettings(const QString& path);
    BundleSettings(void);

    ~BundleSettings() = default;

    std::vector<std::vector<std::vector<float>>> getBundle() const;
    std::vector<uint32_t> getnPointsBundle() const;

    void setBundle(std::vector<std::vector<std::vector<float>>> bundle);
    void setNPointsBundle(std::vector<uint32_t> nPoints);

private:
    std::vector<uint32_t> m_nPointsBundle;
    std::vector<std::vector<std::vector<float>>> m_bundle;
};

#endif // BUNDLE_SETTINGS_H
