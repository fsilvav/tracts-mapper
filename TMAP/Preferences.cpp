#include "Preferences.h"
#include "ui_preferences.h"

// QT
#include <QDialog>
#include <QDialogButtonBox>

Preferences::Preferences(bool state, QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::Preferences)
    , m_state(state)
{
    ui->setupUi(this);

    connect(ui->buttonBox, &QDialogButtonBox::clicked, this, &Preferences::buttonClicled);
    connect(ui->BundlesRenCheckBox, &QCheckBox::toggled, this, &Preferences::setState);

    ui->BundlesRenCheckBox->setChecked(m_state);
}

void Preferences::buttonClicled(QAbstractButton* button)
{
    if (!button) {
        return;
    }

    QDialogButtonBox::ButtonRole role = ui->buttonBox->buttonRole(button);

    if (role == QDialogButtonBox::AcceptRole) {
        accept();
    } else if (role == QDialogButtonBox::RejectRole) {
        reject();
    }
}

void Preferences::setState(bool on)
{
    m_state = on;
}

bool Preferences::getState()
{
    return m_state;
}

Preferences::~Preferences()
{
    delete ui;
}
