#ifndef MESH_SETTINGS_H
#define MESH_SETTINGS_H

// QT
#include <QString>
#include <QVector>

class MeshSettings {
public:
    MeshSettings(const QString& path);
    ~MeshSettings() = default;

    QVector<QVector<float>> getVertices() const;
    QVector<QVector<uint32_t>> getIndices() const;

private:
    QVector<QVector<float>> m_vertices;
    QVector<QVector<uint32_t>> m_indices;
};

#endif // MESH_SETTINGS_H
