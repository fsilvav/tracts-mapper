#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QDialog>

class QAbstractButton;

namespace Ui {
class Preferences;
}

class Preferences : public QDialog {
    Q_OBJECT

public:
    explicit Preferences(bool state, QWidget* parent = nullptr);
    ~Preferences() override;

    bool getState();

private:
    Ui::Preferences* ui;

    bool m_state = false;

private slots:
    void buttonClicled(QAbstractButton* button);
    void setState(bool on);
};

#endif // PREFERENCES_H
