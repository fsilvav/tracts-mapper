#-------------------------------------------------
#
# Project created by QtCreator 2020-01-20T22:09:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TMAP
TEMPLATE = app

CONFIG += c++14 console
CONFIG -= app_bundle

QMAKE_CXXFLAGS += -fopenmp
LIBS += -fopenmp
QMAKE_CFLAGS = -c -Wall -O3

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        FileReader.cpp \
        MeshSettings.cpp \
        BundleSettings.cpp \
        TMapModel.cpp \
        TMapModelWidgets.cpp \
        MainWindow.cpp \
    Preferences.cpp

HEADERS += \
        FileReader.h \
        MeshSettings.h \
        BundleSettings.h \
        TMapModel.h \
        TMapModelWidgets.h \
        MainWindow.h \
    Preferences.h

FORMS += \
        mainwindow.ui \
        tmapmodelwidgets.ui \
    preferences.ui

INCLUDEPATH += /usr/include/x86_64-linux-gnu/qt5/QtGui/
INCLUDEPATH += /usr/include/x86_64-linux-gnu/qt5/QtWidgets/

INCLUDEPATH += /usr/local/include/vtk-9.0/

LIBS += -L/usr/local/lib/vtk-9.0.1/

LIBS += -lvtkChartsCore-9.0\
        -lvtkCommonColor-9.0\
        -lvtkCommonComputationalGeometry-9.0\
        -lvtkCommonCore-9.0\
        -lvtkCommonDataModel-9.0\
        -lvtkCommonExecutionModel-9.0\
        -lvtkCommonMath-9.0\
        -lvtkCommonMisc-9.0\
        -lvtkCommonSystem-9.0\
        -lvtkCommonTransforms-9.0\
        -lvtkDICOMParser-9.0\
        -lvtkDomainsChemistry-9.0\
        -lvtkdoubleconversion-9.0\
        -lvtkexodusII-9.0\
        -lvtkexpat-9.0\
        -lvtkFiltersAMR-9.0\
        -lvtkFiltersCore-9.0\
        -lvtkFiltersExtraction-9.0\
        -lvtkFiltersFlowPaths-9.0\
        -lvtkFiltersGeneral-9.0\
        -lvtkFiltersGeneric-9.0\
        -lvtkFiltersGeometry-9.0\
        -lvtkFiltersHybrid-9.0\
        -lvtkFiltersHyperTree-9.0\
        -lvtkFiltersImaging-9.0\
        -lvtkFiltersModeling-9.0\
        -lvtkFiltersParallel-9.0\
        -lvtkFiltersParallelImaging-9.0\
        -lvtkFiltersPoints-9.0\
        -lvtkFiltersProgrammable-9.0\
        -lvtkFiltersSelection-9.0\
        -lvtkFiltersSMP-9.0\
        -lvtkFiltersSources-9.0\
        -lvtkFiltersStatistics-9.0\
        -lvtkFiltersTexture-9.0\
        -lvtkFiltersTopology-9.0\
        -lvtkFiltersVerdict-9.0\
        -lvtkfreetype-9.0\
        -lvtkGeovisCore-9.0\
        -lvtkgl2ps-9.0\
        -lvtkglew-9.0\
        -lvtkGUISupportQt-9.0\
        -lvtkGUISupportQtSQL-9.0\
        -lvtkhdf5-9.0\
        -lvtkhdf5_hl-9.0\
        -lvtkImagingColor-9.0\
        -lvtkImagingCore-9.0\
        -lvtkImagingFourier-9.0\
        -lvtkImagingGeneral-9.0\
        -lvtkImagingHybrid-9.0\
        -lvtkImagingMath-9.0\
        -lvtkImagingMorphological-9.0\
        -lvtkImagingSources-9.0\
        -lvtkImagingStatistics-9.0\
        -lvtkImagingStencil-9.0\
        -lvtkInfovisCore-9.0\
        -lvtkInfovisLayout-9.0\
        -lvtkInteractionImage-9.0\
        -lvtkInteractionStyle-9.0\
        -lvtkInteractionWidgets-9.0\
        -lvtkIOAMR-9.0\
        -lvtkIOAsynchronous-9.0\
        -lvtkIOCityGML-9.0\
        -lvtkIOCore-9.0\
        -lvtkIOEnSight-9.0\
        -lvtkIOExodus-9.0\
        -lvtkIOExport-9.0\
        -lvtkIOExportGL2PS-9.0\
        -lvtkIOExportPDF-9.0\
        -lvtkIOGeometry-9.0\
        -lvtkIOImage-9.0\
        -lvtkIOImport-9.0\
        -lvtkIOInfovis-9.0\
        -lvtkIOLegacy-9.0\
        -lvtkIOLSDyna-9.0\
        -lvtkIOMINC-9.0\
        -lvtkIOMotionFX-9.0\
        -lvtkIOMovie-9.0\
        -lvtkIONetCDF-9.0\
        -lvtkIOOggTheora-9.0\
        -lvtkIOParallel-9.0\
        -lvtkIOParallelXML-9.0\
        -lvtkIOPLY-9.0\
        -lvtkIOSegY-9.0\
        -lvtkIOSQL-9.0\
        -lvtkIOTecplotTable-9.0\
        -lvtkIOVeraOut-9.0\
        -lvtkIOVideo-9.0\
        -lvtkIOXML-9.0\
        -lvtkIOXMLParser-9.0\
        -lvtkjpeg-9.0\
        -lvtkjsoncpp-9.0\
        -lvtklibharu-9.0\
        -lvtklibproj-9.0\
        -lvtklibxml2-9.0\
        -lvtkloguru-9.0\
        -lvtklz4-9.0\
        -lvtklzma-9.0\
        -lvtkmetaio-9.0\
        -lvtknetcdf-9.0\
        -lvtkogg-9.0\
        -lvtkParallelCore-9.0\
        -lvtkParallelDIY-9.0\
        -lvtkpng-9.0\
        -lvtkpugixml-9.0\
        -lvtkRenderingAnnotation-9.0\
        -lvtkRenderingContext2D-9.0\
        -lvtkRenderingCore-9.0\
        -lvtkRenderingFreeType-9.0\
        -lvtkRenderingGL2PSOpenGL2-9.0\
        -lvtkRenderingImage-9.0\
        -lvtkRenderingLabel-9.0\
        -lvtkRenderingLOD-9.0\
        -lvtkRenderingOpenGL2-9.0\
        -lvtkRenderingQt-9.0\
        -lvtkRenderingSceneGraph-9.0\
        -lvtkRenderingUI-9.0\
        -lvtkRenderingVolume-9.0\
        -lvtkRenderingVolumeOpenGL2-9.0\
        -lvtkRenderingVtkJS-9.0\
        -lvtksqlite-9.0\
        -lvtksys-9.0\
        -lvtkTestingRendering-9.0\
        -lvtktheora-9.0\
        -lvtktiff-9.0\
        -lvtkverdict-9.0\
        -lvtkViewsContext2D-9.0\
        -lvtkViewsCore-9.0\
        -lvtkViewsInfovis-9.0\
        -lvtkViewsQt-9.0\
        -lvtkWrappingTools-9.0\
        -lvtkzlib-9.0\
