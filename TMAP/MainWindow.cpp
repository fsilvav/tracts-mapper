#include "MainWindow.h"
#include "FileReader.h"
#include "Preferences.h"
#include "TMapModelWidgets.h"
#include "ui_mainwindow.h"

// QT
#include <QFileDialog>
#include <QGridLayout>
#include <QKeyEvent>
#include <QWidget>

// C++
#include <iostream>

// VTK
#include <vtkActor.h>
#include <vtkAppendPolyData.h>
#include <vtkCamera.h>
#include <vtkCellArray.h>
#include <vtkCleanPolyData.h>
#include <vtkFloatArray.h>
#include <vtkLine.h>
#include <vtkNamedColors.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkUnsignedCharArray.h>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , m_ui(new Ui::MainWindow)
{
    m_ui->setupUi(this);

    connect(m_ui->importQAction, &QAction::triggered, this, &MainWindow::importFile);
    connect(m_ui->exitQAction, &QAction::triggered, this, &MainWindow::exitApp);
    connect(m_ui->PreferencesQAction, &QAction::triggered, this, &MainWindow::preferences);
}

MainWindow::~MainWindow()
{
    delete m_ui;
}

void MainWindow::importFile()
{
    QStringList paths = QFileDialog::getOpenFileNames(this,
        tr("Select one or more files to open"), "", tr("All files (*.mesh *.bundlesdata);;"
                                                       "Mesh files (*.mesh);;"
                                                       "Bundles files (*.bundlesdata)"));
    using Ext = FileReader::FileExt;

    int i = 1;
    for (const QString& path : paths) {

        FileReader* file = new FileReader(path);

        switch (file->getFileExt()) {

        case Ext::MESH: {
            MeshSettings* mesh = new MeshSettings(path);
            m_meshes.append(mesh);
            addMeshRenderer(mesh);
            break;
        }
        case Ext::BUNDLESDATA: {
            BundleSettings* bundle = new BundleSettings(path);
            m_bundles.append(bundle);

            if (m_stateBundles) {
                std::cout << "Adding bundle: " << i << "/" << paths.size() << "...";
                addBundleRenderer(bundle);
                std::cout << " done!" << std::endl;
                ++i;
            }
            break;
        }
        case Ext::NONE: {
            // TODO: Warning message
            break;
        }
        }
    }

    if (!m_meshes.isEmpty() || !m_bundles.isEmpty()) {
        runVTKWidget();
    }
}

void MainWindow::addMeshRenderer(MeshSettings* mesh)
{
    QVector<QVector<float>> vertices = mesh->getVertices();
    QVector<QVector<uint32_t>> indices = mesh->getIndices();

    //    std::array<std::array<double, 3>, 8> pts;
    QVector<std::array<vtkIdType, 3>> ordering;

    for (int i = 0; i < indices.count(); ++i) {
        std::array<vtkIdType, 3> ind = { indices[i][0], indices[i][1], indices[i][2] };
        ordering.push_back(ind);
    }

    auto polyData = vtkSmartPointer<vtkPolyData>::New();
    auto points = vtkSmartPointer<vtkPoints>::New();
    auto polys = vtkSmartPointer<vtkCellArray>::New();
    auto scalars = vtkSmartPointer<vtkFloatArray>::New();

    for (int i = 0; i < vertices.count(); ++i) {
        double v[3] = { double(vertices[i][0]), double(vertices[i][1]), double(vertices[i][2]) };
        points->InsertNextPoint(v);
        scalars->InsertTuple1(vtkIdType(i), i);
    }

    for (auto&& i : ordering) {
        polys->InsertNextCell(vtkIdType(i.size()), i.data());
    }

    polyData->SetPoints(points);
    polyData->SetPolys(polys);
    //    polyData->GetPointData()->SetScalars(scalars);

    // VTK Mapper
    auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputData(polyData);
    mapper->SetScalarRange(polyData->GetScalarRange());

    // VTK Actor
    auto actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    m_meshActors.append(actor);
    //    actor->GetProperty()->SetColor(1.0, 0.0, 0.0);

    // VTK Camera
    auto camera = vtkSmartPointer<vtkCamera>::New();
    camera->SetPosition(1, 1, 1);
    camera->SetFocalPoint(0, 0, 0);

    // VTK Renderer
    m_renderer->AddActor(actor);
    m_renderer->SetActiveCamera(camera);
    m_renderer->ResetCamera();
}

void MainWindow::addBundleRenderer(BundleSettings* bundle)
{
    std::vector<uint32_t> lens = bundle->getnPointsBundle();
    std::vector<std::vector<std::vector<float>>> fibers = bundle->getBundle();

    //Append vtkPolyData
    auto appendFilter = vtkSmartPointer<vtkAppendPolyData>::New();

    for (uint32_t i = 0; i < bundle->getBundle().size(); ++i) {
        appendFilter->AddInputData(getVtkPolydataFiber(lens[i], fibers[i]));
    }

    // Remove any duplicate points.
    auto cleanFilter = vtkSmartPointer<vtkCleanPolyData>::New();
    cleanFilter->SetInputConnection(appendFilter->GetOutputPort());
    cleanFilter->Update();

    auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(cleanFilter->GetOutputPort());
    mapper->SetScalarRange(getVtkPolydataFiber(lens[0], fibers[0])->GetScalarRange());

    auto actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    actor->SetVisibility(m_bundlesActive);

    m_bundleActors.append(actor);

    m_renderer->AddActor(actor);
    m_renderer->ResetCamera();
}

vtkSmartPointer<vtkPolyData> MainWindow::getVtkPolydataFiber(uint32_t len, std::vector<std::vector<float>> fiber)
{
    // Create a vtkPoints container and store the points in it
    auto pts = vtkSmartPointer<vtkPoints>::New();
    auto scalars = vtkSmartPointer<vtkFloatArray>::New();

    for (uint32_t i = 0; i < len; ++i) {
        double v[3] = { double(fiber[i][0]), double(fiber[i][1]), double(fiber[i][2]) };
        pts->InsertNextPoint(v);
        scalars->InsertTuple1(vtkIdType(i), i);
    }

    // Create a vtkCellArray container and store the lines in it
    auto lines = vtkSmartPointer<vtkCellArray>::New();

    for (unsigned int i = 0; i < len - 1; ++i) {
        vtkSmartPointer<vtkLine> line = vtkSmartPointer<vtkLine>::New();
        line->GetPointIds()->SetId(0, i);
        line->GetPointIds()->SetId(1, i + 1);
        lines->InsertNextCell(line);
    }

    // Create the polydata where we will store all the geometric data
    auto linesPolyData = vtkSmartPointer<vtkPolyData>::New();
    // Add the points to the polydata container
    linesPolyData->SetPoints(pts);

    // Add the lines to the polydata container
    linesPolyData->SetLines(lines);
    linesPolyData->GetPointData()->SetScalars(scalars);

    return linesPolyData;

    // Setup the visualization pipeline
    //    vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    //    mapper->SetInputData(linesPolyData);
    //    mapper->SetScalarRange(linesPolyData->GetScalarRange());

    //    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    //    actor->SetMapper(mapper);

    //    m_renderer->AddActor(actor);
}

void MainWindow::runVTKWidget()
{
    auto colors = vtkSmartPointer<vtkNamedColors>::New();
    //    m_renderer->SetBackground(colors->GetColor3d("Cornsilk").GetData());
    m_renderer->SetBackground(1.0, 1.0, 1.0);

    if (!m_qvtkWidget) {
        m_qvtkWidget = new QVTKOpenGLNativeWidget(this);
        m_qvtkWidget->GetRenderWindow()->AddRenderer(m_renderer);
        //        this->setCentralWidget(m_qvtkWidget);
    }

    m_qvtkWidget->update();
    m_qvtkWidget->GetRenderWindow()->Render();
    m_qvtkWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QWidget* widget = new QWidget(this);
    QGridLayout* layout = new QGridLayout(widget);

    this->setCentralWidget(widget);
    widget->setLayout(layout);

    m_tmapMW = new TMapModelWidgets(widget, m_meshes, m_bundles);

    layout->addWidget(m_tmapMW, 0, 0);

    connect(m_tmapMW, &TMapModelWidgets::sliderChanged, this, &MainWindow::setMeshOpacity);
    connect(m_tmapMW, &TMapModelWidgets::bundlesActive, this, &MainWindow::setBundlesActive);
    connect(m_tmapMW, &TMapModelWidgets::sendInTri, this, &MainWindow::setInTri);
    connect(m_tmapMW, &TMapModelWidgets::sendFnTri, this, &MainWindow::setFnTri);

    layout->addWidget(m_qvtkWidget, 0, 1);
}

void MainWindow::setMeshOpacity(int value)
{
    double op = (10 - value) / 10.0;

    for (vtkSmartPointer<vtkActor> actor : m_meshActors) {
        actor->GetProperty()->SetOpacity(op);
        m_renderer->AddActor(actor);
    }

    m_qvtkWidget->update();
    m_qvtkWidget->GetRenderWindow()->Render();
}

void MainWindow::setBundlesActive(bool on)
{
    m_bundlesActive = on;

    for (vtkSmartPointer<vtkActor> actor : m_bundleActors) {
        actor->SetVisibility(on);
        m_renderer->AddActor(actor);
        m_renderer->ResetCamera();
    }

    m_qvtkWidget->update();
    m_qvtkWidget->GetRenderWindow()->Render();
}

template <typename T>
std::vector<double> linspace(T start_in, T end_in, int num_in)
{

    std::vector<double> linspaced;

    double start = static_cast<double>(start_in);
    double end = static_cast<double>(end_in);
    double num = static_cast<double>(num_in);

    if (num == 0) {
        return linspaced;
    }
    if (num == 1) {
        linspaced.push_back(start);
        return linspaced;
    }

    double delta = (end - start) / (num - 1);

    for (int i = 0; i < num - 1; ++i) {
        linspaced.push_back(start + delta * i);
    }
    linspaced.push_back(end); // I want to ensure that start and end
        // are exactly the same as the input
    return linspaced;
}

void MainWindow::setInTri(std::vector<std::vector<uint32_t>> InTri)
{
    QVector<QVector<double>> newColors, colors;

    double N = 1.0;
    while ((std::pow(4, N) - 3) < InTri.size()) {
        N += 1;
    }

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            for (int k = 0; k < N; ++k) {
                newColors.append(QVector<double>{ double(i + 1) / N, double(j + 1) / N, double(k + 1) / N });
            }
        }
    }

    std::vector<double> inds = linspace(2, newColors.size() - 2, int(InTri.size()));

    for (size_t i = 0; i < inds.size(); ++i) {
        colors.append(newColors[int(inds[i])]);
    }

    m_InTri = InTri;

    QVector<QVector<float>> vertices = m_meshes[0]->getVertices();
    QVector<QVector<uint32_t>> indices = m_meshes[0]->getIndices();

    for (uint32_t i = 0; i < InTri.size(); ++i) {

        QVector<std::array<vtkIdType, 3>> ordering;

        std::cout << "Adding region: " << (i + 1) << "/" << InTri.size() << "..." << std::endl;

        for (uint32_t j = 0; j < InTri[i].size(); ++j) {

            uint32_t ind = InTri[i][j];

            uint32_t a = indices[ind][0];
            uint32_t b = indices[ind][1];
            uint32_t c = indices[ind][2];

            std::array<vtkIdType, 3> indexes = { a, b, c };
            ordering.push_back(indexes);
        }

        auto polyData = vtkSmartPointer<vtkPolyData>::New();
        auto points = vtkSmartPointer<vtkPoints>::New();
        auto polys = vtkSmartPointer<vtkCellArray>::New();
        auto scalars = vtkSmartPointer<vtkFloatArray>::New();

        for (int i = 0; i < vertices.count(); ++i) {
            double v[3] = { double(vertices[i][0]), double(vertices[i][1]), double(vertices[i][2]) };
            points->InsertNextPoint(v);
            scalars->InsertTuple1(vtkIdType(i), i);
        }

        for (auto&& i : ordering) {
            polys->InsertNextCell(vtkIdType(i.size()), i.data());
        }

        polyData->SetPoints(points);
        polyData->SetPolys(polys);

        // VTK Mapper
        auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInputData(polyData);
        mapper->SetScalarRange(polyData->GetScalarRange());

        // VTK Actor
        auto actor = vtkSmartPointer<vtkActor>::New();
        actor->SetMapper(mapper);
        //    m_meshActors.append(actor);
        //        actor->GetProperty()->SetColor(1.0, 0.0, 0.0);
        actor->GetProperty()->SetColor(colors[i][0], colors[i][1], colors[i][2]);

        // VTK Renderer
        m_renderer->AddActor(actor);
    }
    m_renderer->ResetCamera();

    m_qvtkWidget->update();
    m_qvtkWidget->GetRenderWindow()->Render();
}

void MainWindow::setFnTri(std::vector<std::vector<uint32_t>> FnTri)
{
    QVector<QVector<double>> newColors, colors;

    double N = 1.0;
    while ((std::pow(4, N) - 3) < FnTri.size()) {
        N += 1;
    }

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            for (int k = 0; k < N; ++k) {
                newColors.append(QVector<double>{ double(i + 1) / N, double(j + 1) / N, double(k + 1) / N });
            }
        }
    }

    std::vector<double> inds = linspace(2, newColors.size() - 2, int(FnTri.size()));

    for (size_t i = 0; i < inds.size(); ++i) {
        colors.append(newColors[int(inds[i])]);
    }

    m_FnTri = FnTri;

    QVector<QVector<float>> vertices = m_meshes[0]->getVertices();
    QVector<QVector<uint32_t>> indices = m_meshes[0]->getIndices();

    for (uint32_t i = 0; i < FnTri.size(); ++i) {

        QVector<std::array<vtkIdType, 3>> ordering;

        std::cout << "Adding region: " << (i + 1) << "/" << FnTri.size() << "..." << std::endl;

        for (uint32_t j = 0; j < FnTri[i].size(); ++j) {

            uint32_t ind = FnTri[i][j];

            uint32_t a = indices[ind][0];
            uint32_t b = indices[ind][1];
            uint32_t c = indices[ind][2];

            std::array<vtkIdType, 3> indexes = { a, b, c };
            ordering.push_back(indexes);
        }

        auto polyData = vtkSmartPointer<vtkPolyData>::New();
        auto points = vtkSmartPointer<vtkPoints>::New();
        auto polys = vtkSmartPointer<vtkCellArray>::New();
        auto scalars = vtkSmartPointer<vtkFloatArray>::New();

        for (int i = 0; i < vertices.count(); ++i) {
            double v[3] = { double(vertices[i][0]), double(vertices[i][1]), double(vertices[i][2]) };
            points->InsertNextPoint(v);
            scalars->InsertTuple1(vtkIdType(i), i);
        }

        for (auto&& i : ordering) {
            polys->InsertNextCell(vtkIdType(i.size()), i.data());
        }

        polyData->SetPoints(points);
        polyData->SetPolys(polys);

        // VTK Mapper
        auto mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInputData(polyData);
        mapper->SetScalarRange(polyData->GetScalarRange());

        // VTK Actor
        auto actor = vtkSmartPointer<vtkActor>::New();
        actor->SetMapper(mapper);
        //    m_meshActors.append(actor);
        //        actor->GetProperty()->SetColor(1.0, 0.0, 0.0);
        actor->GetProperty()->SetColor(colors[i][0], colors[i][1], colors[i][2]);

        // VTK Renderer
        m_renderer->AddActor(actor);
    }
    m_renderer->ResetCamera();

    m_qvtkWidget->update();
    m_qvtkWidget->GetRenderWindow()->Render();
}

void MainWindow::preferences()
{
    Preferences dialog(m_stateBundles, this);

    int ok = dialog.exec();

    if (ok) {

        m_stateBundles = dialog.getState();
    }
}

void MainWindow::exitApp()
{
    qApp->exit();
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
    if (event->type() == QEvent::KeyPress) {

        if (event->modifiers().testFlag(Qt::ControlModifier)) {

            switch (event->key()) {

            case Qt::Key_O:
                importFile();
                break;
            }
        }
    }
}
