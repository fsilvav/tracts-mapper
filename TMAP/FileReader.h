#ifndef FILE_READER_H
#define FILE_READER_H

// QT
#include <QString>

class FileReader {

public:
    enum class FileExt {
        MESH = 0,
        BUNDLESDATA,
        NONE
    };

    FileReader(const QString& path);
    ~FileReader() = default;

    FileExt getFileExt() const;

protected:
    void defineExt();

private:
    QString m_path = QString();
    FileExt m_fileExt = FileExt::NONE;
};

#endif // FILE_READER_H
