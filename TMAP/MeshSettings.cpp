#include "MeshSettings.h"

// C++
#include <fstream>

MeshSettings::MeshSettings(const QString& path)
{
    std::ifstream file(path.toStdString(), std::ios::in | std::ios::binary);

    if (!file.is_open()) {
        return;
    }

    //    float** vertex;
    //    uint32_t** polygons;
    uint32_t len_vertex;
    uint32_t len_polygons;

    file.seekg(17);

    uint32_t dim;

    file.read((char*)&dim, sizeof(uint32_t)); // dimensiones

    file.seekg(29);

    file.read((char*)&len_vertex, sizeof(uint32_t)); // largo de vértices

    //    vertex = new float*[len_vertex];
    m_vertices.resize(len_vertex);
    //    QVector<QVector<float>> vertex(len_vertex);

    for (uint32_t i = 0; i < len_vertex; i++) {
        //        vertex[i] = new float[3];

        for (uint8_t j = 0; j < 3; j++) {
            float v;
            file.read((char*)&v, sizeof(float)); // lee cada punto (x,y,z)
            m_vertices[i].append(v);
        }
    }

    file.seekg(4 * 3 * len_vertex + 41);

    file.read((char*)&len_polygons, sizeof(uint32_t)); // largo de polígonos
    //    polygons = new uint32_t*[len_polygons];
    m_indices.resize(len_polygons);

    for (uint32_t i = 0; i < len_polygons; i++) {
        //        polygons[i] = new uint32_t[3];

        for (uint8_t j = 0; j < 3; j++) {
            uint32_t p;
            file.read((char*)&p, sizeof(uint32_t)); // lee cada índice del triángulo (a,b,c)
            m_indices[i].append(p);
        }
    }

    file.close();
}

QVector<QVector<float>> MeshSettings::getVertices() const
{
    return m_vertices;
}

QVector<QVector<uint32_t>> MeshSettings::getIndices() const
{
    return m_indices;
}
