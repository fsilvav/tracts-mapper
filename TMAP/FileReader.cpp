#include "FileReader.h"

// QT
#include <QFileInfo>

FileReader::FileReader(const QString& path)
    : m_path(path)
{
    defineExt();
}

void FileReader::defineExt()
{
    QString ext = QFileInfo(m_path).suffix();

    if (ext == "mesh") {
        m_fileExt = FileExt::MESH;

    } else if (ext == "bundlesdata") {
        m_fileExt = FileExt::BUNDLESDATA;

    } else {
        m_fileExt = FileExt::NONE;
    }
}

FileReader::FileExt FileReader::getFileExt() const
{
    return m_fileExt;
}
