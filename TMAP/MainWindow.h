#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "BundleSettings.h"
#include "MeshSettings.h"

// QT
#include <QList>
#include <QMainWindow>
#include <QVTKOpenGLNativeWidget.h>

// VTK
#include <vtkPolyData.h>
#include <vtkRenderer.h>

class Preferences;
class TMapModelWidgets;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

    void addMeshRenderer(MeshSettings* mesh = nullptr);
    void addBundleRenderer(BundleSettings* bundle = nullptr);
    vtkSmartPointer<vtkPolyData> getVtkPolydataFiber(uint32_t len, std::vector<std::vector<float>> bundle);
    void runVTKWidget();

private slots:
    void importFile();
    void preferences();
    void exitApp();
    void setMeshOpacity(int value);
    void setBundlesActive(bool on);
    void setInTri(std::vector<std::vector<uint32_t>> InTri);
    void setFnTri(std::vector<std::vector<uint32_t>> FnTri);

private:
    Ui::MainWindow* m_ui;

    QList<MeshSettings*> m_meshes;
    QList<BundleSettings*> m_bundles;

    QList<vtkSmartPointer<vtkActor>> m_meshActors;
    QList<vtkSmartPointer<vtkActor>> m_bundleActors;

    vtkSmartPointer<vtkRenderer> m_renderer = vtkSmartPointer<vtkRenderer>::New();
    QVTKOpenGLNativeWidget* m_qvtkWidget = nullptr;
    TMapModelWidgets* m_tmapMW = nullptr;
    Preferences* m_preferences = nullptr;

    bool m_bundlesActive = false;
    bool m_stateBundles = false;

    std::vector<std::vector<uint32_t>> m_InTri, m_FnTri;

    void keyPressEvent(QKeyEvent* event);
};

template <typename T>
std::vector<double> linspace(T start_in, T end_in, int num_in);

#endif // MAIN_WINDOW_H
