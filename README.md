# Tracts Mapper

### Install Qt and Qt Creator:

```sh
$ sudo apt-get install build-essential qtcreator qt5-default
```

### Install some packages to build VTK properly:

```sh
$ sudo apt-get install libxt-dev libqt5x11extras5-dev qttools5-dev
```

Download VTK 9 source from [download site](http://www.vtk.org/download/) into a local directory and unzip it. Suppose the unzipped source directory is `/home/me/Downloads/VTK-9.0.1`.

Create a separate build directory, e.g., `/opt/VTK-build`:

```sh
$ sudo su
$ mkdir /opt/VTK-build
$ cd /opt/VTK-build
$ ccmake /home/me/Downloads/VTK-9.0.1
```

If you need to install ccmake, do:

```sh
$ apt install cmake-curses-gui
```

Type `c` to configure the following variables:

| Variable | State |
| ------ | ------ |
| BUILD_SHARED_LIBS | ON |
| CMAKE_BUILD_TYPE | Release |
| VTK_BUILD_TESTING | ON |
| VTK_GROUP_ENABLE_Qt | YES |
| VTK_GROUP_ENABLE_Rendering | YES |
| VTK_GROUP_ENABLE_StandAlone | YES |

Type `t` to switch to advance mode and configure one more variable:

| Variable | State |
| ------ | ------ |
| VTK_MODULE_ENABLE_VTK_GUISupportQt | YES |

Type `c` one or more times until no errors occur. Then, type `g` to generate.

### From the build directory, do:

```sh
$ sudo make -j<number of cores> clean
$ sudo make -j<number of cores>
$ sudo make -j<number of cores> install
```

By default, the .h files are installed in `/usr/local/include/vtk-9.0` and the library files are installed in `/usr/local/lib`. To be consistent you can create a directory `/usr/local/lib/vtk-9.0` and move the library files into this directory:

```sh
$ sudo su
$ mkdir /usr/local/lib/vtk-9.0.1
$ cd /usr/local/lib
$ mv libvtk* /usr/local/lib/vtk-9.0.1
```

### Clone the project

```sh
$ git clone https://gitlab.com/fsilvav/tracts-mapper.git
```

Open Qt Creator and open a new project. Then, select the `.pro` file from the TMAP folder and click on `Configure Project`.

### Clang-format
- Open Qt Creator. Go to **Help** -> **About Plugins...**
- Enable *'Beautifier'* plugin.
- Disable *'ClangCodeModel'* plugin and restart Qt Creator.
- Install clang-format:

```sh
$ sudo apt-get install clang-format
```

##### Setting Clang-Format in Qt Creator

- Go to **Tools** -> **Options...** -> **Beautifier** -> **Clang Format**.
- In Configuration, select the clang-format file from: `/usr/bin/clang-format`.
- In Options, select the **WebKit** style. Then click on **Use customized style** and add the following value:
```sh
ColumnLimit:    80
```
This will set a maximum of 80 characters as the line length.

##### Adding a ShortCut

- Go to **Tools** -> **Options...** -> **Environment** -> **Keyboard**.
- Type `format` as filter. Then, select **FormatAtCursor** and set a shortcur, e.g., *`Ctrl + Shift + K`*.
- Apply and OK. Now you can start programming!.
